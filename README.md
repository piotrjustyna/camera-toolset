# README #

This is a toolset for Canon cameras supporting EDSDK library. There's not much going on here right now, but my ultimate goal is to implement a focusing algorithm allowing DSLR users to take photos in poor light conditions (e.g. night or star photography). To build this project, you will need to become a member of Canon Developer Programme and obtain the EDSDK libraries - more info here: [https://www.developers.canon-europa.com](https://www.developers.canon-europa.com).

*Main application window*:

![16.png](https://bitbucket.org/repo/enEe8o/images/4012362946-16.png)

### What is this repository for? ###

* I wanted to interface my Canon 450D DSLR and possibly write/share something useful.
* Current version supports:
    * detecting connected cameras and their properties:
        * aperture
        * image quality
        * iso
        * jpeg quality
        * name
        * storage volumes
        * live view status
    * responding to and interpreting selected property events:
        * aperture
        * image quality
        * iso
        * jpeg quality
        * live view status
    * starting and stopping the live view
    * taking pictures
### How do I get set up? ###

* Become a member of Canon Developer Programme: [https://www.developers.canon-europa.com](https://www.developers.canon-europa.com)
* Once you're registered, download the EDSDK libraries
* Create a "Libraries" folder in the solution's folder:
```
#!shell

CameraToolset\Libraries
```
* Copy relevant EDSDK dll's to that folder
* Build

### Contact ###

Questions, ideas, suggestions? Find me on twitter: [@LetZweindaut](https://twitter.com/LetZweindaut).