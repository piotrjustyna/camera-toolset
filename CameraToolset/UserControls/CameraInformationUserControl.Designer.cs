﻿namespace CameraToolset.UserControls
{
    partial class CameraInformationUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CameraInformationPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.SuspendLayout();
            // 
            // CameraInformationPropertyGrid
            // 
            this.CameraInformationPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CameraInformationPropertyGrid.Location = new System.Drawing.Point(0, 0);
            this.CameraInformationPropertyGrid.Name = "CameraInformationPropertyGrid";
            this.CameraInformationPropertyGrid.Size = new System.Drawing.Size(800, 600);
            this.CameraInformationPropertyGrid.TabIndex = 1;
            this.CameraInformationPropertyGrid.ToolbarVisible = false;
            // 
            // CameraInformationUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CameraInformationPropertyGrid);
            this.Name = "CameraInformationUserControl";
            this.Size = new System.Drawing.Size(800, 600);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PropertyGrid CameraInformationPropertyGrid;


    }
}
