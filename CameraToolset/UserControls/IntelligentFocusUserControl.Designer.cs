﻿namespace CameraToolset.UserControls
{
    partial class IntelligentFocusUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IntelligentFocusTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.FocusButton = new System.Windows.Forms.Button();
            this.IntelligentFocusTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // IntelligentFocusTableLayoutPanel
            // 
            this.IntelligentFocusTableLayoutPanel.ColumnCount = 1;
            this.IntelligentFocusTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.IntelligentFocusTableLayoutPanel.Controls.Add(this.FocusButton, 0, 1);
            this.IntelligentFocusTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IntelligentFocusTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.IntelligentFocusTableLayoutPanel.Name = "IntelligentFocusTableLayoutPanel";
            this.IntelligentFocusTableLayoutPanel.RowCount = 2;
            this.IntelligentFocusTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.IntelligentFocusTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.IntelligentFocusTableLayoutPanel.Size = new System.Drawing.Size(300, 150);
            this.IntelligentFocusTableLayoutPanel.TabIndex = 0;
            // 
            // FocusButton
            // 
            this.FocusButton.AutoSize = true;
            this.FocusButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FocusButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FocusButton.Location = new System.Drawing.Point(3, 124);
            this.FocusButton.Name = "FocusButton";
            this.FocusButton.Size = new System.Drawing.Size(294, 23);
            this.FocusButton.TabIndex = 0;
            this.FocusButton.Text = "Take Sample Picture";
            this.FocusButton.UseVisualStyleBackColor = true;
            this.FocusButton.Click += new System.EventHandler(this.FocusButton_Click);
            // 
            // IntelligentFocusUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.IntelligentFocusTableLayoutPanel);
            this.Name = "IntelligentFocusUserControl";
            this.Size = new System.Drawing.Size(300, 150);
            this.IntelligentFocusTableLayoutPanel.ResumeLayout(false);
            this.IntelligentFocusTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel IntelligentFocusTableLayoutPanel;
        private System.Windows.Forms.Button FocusButton;
    }
}
