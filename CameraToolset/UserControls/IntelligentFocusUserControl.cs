﻿using CameraToolset.Forms;
using CameraToolset.Model;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace CameraToolset.UserControls
{
    public partial class IntelligentFocusUserControl : UserControl
    {
        public delegate void PictureTakenEventHandler(Image picture);

        public PictureTakenEventHandler PictureTakenHandler { get; set; }

        public Camera SelectedCamera { get; set; }

        public IntelligentFocusUserControl()
        {
            InitializeComponent();
        }

        private void FocusButton_Click(object sender, EventArgs e)
        {
            OperationResult result = OperationResultHandler.HandleOperationResult(CanonSDK.SendTakePictureCommand(SelectedCamera));

            if (result.IsSuccess)
            {
                // "take picture" command sent successfully
            }
            else
            {
                // could not send the "take picture" command
            }
        }

        public void HandleChangingDirectoryItem(IntPtr directoryItemReference)
        {
            PictureDirectoryItemInfo pictureInfo = null;

            if (CanonSDK.IsChangingDirectoryItemAPicture(directoryItemReference, out pictureInfo))
            {
                // let's retrieve it!
                Image picture = null;

                OperationResult downloadResult = CanonSDK.DownloadPicture(
                    directoryItemReference,
                    pictureInfo,
                    out picture);

                if (downloadResult.IsSuccess)
                {
                    if (PictureTakenHandler != null)
                    {
                        PictureTakenHandler(picture);
                    }
                    else
                    {
                        // nobody listens to this event
                    }
                }
                else
                {
                    // image could not be downloaded
                }
            }
            else
            {
                // nah, just a folder or unknown item
            }
        }
    }
}
