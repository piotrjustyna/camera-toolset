﻿using CameraToolset.Forms;
using CameraToolset.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CameraToolset.UserControls
{
    public partial class CameraInformationUserControl : UserControl
    {
        private Camera _selectedCamera;

        public Camera SelectedCamera
        {
            get { return _selectedCamera; }
            set 
            { 
                _selectedCamera = value;
                CameraInformationPropertyGrid.SelectedObject = _selectedCamera;
            }
        }

        public CameraInformationUserControl()
        {
            InitializeComponent();
        }

        public void HandleChangingCameraAperture(UInt32 propertyId)
        {
            _selectedCamera.Aperture = CanonSDK.GetCameraAperture(_selectedCamera, propertyId);
            CameraInformationPropertyGrid.SelectedObject = _selectedCamera;
        }

        public void HandleChangingCameraIso(UInt32 propertyId)
        {
            _selectedCamera.Iso = CanonSDK.GetCameraIso(_selectedCamera, propertyId);
            CameraInformationPropertyGrid.SelectedObject = _selectedCamera;
        }

        public void HandleChangingCameraOutputDevice(UInt32 propertyId)
        {
            _selectedCamera.LiveViewOutputDevice = CanonSDK.GetCameraLiveViewOutputDevice(_selectedCamera, propertyId);
            CameraInformationPropertyGrid.SelectedObject = _selectedCamera;
        }

        public void HandleChangingImageQuality(UInt32 propertyId)
        {
            _selectedCamera.ImageQuality = CanonSDK.GetCameraImageQuality(_selectedCamera, propertyId);
            CameraInformationPropertyGrid.SelectedObject = _selectedCamera;
        }

        public void HandleChangingJpegQuality(UInt32 propertyId)
        {
            _selectedCamera.JpegQuality = CanonSDK.GetCameraJpegQuality(_selectedCamera, propertyId);
            CameraInformationPropertyGrid.SelectedObject = _selectedCamera;
        }
    }
}
