﻿namespace CameraToolset.Forms
{
    partial class OperationResultHandler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperationResultHandler));
            this.OperationResultContainerTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.OperationResultDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.OKButton = new System.Windows.Forms.Button();
            this.OperationResultLabel = new System.Windows.Forms.Label();
            this.OperationResultContainerTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // OperationResultContainerTableLayoutPanel
            // 
            this.OperationResultContainerTableLayoutPanel.ColumnCount = 1;
            this.OperationResultContainerTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.OperationResultContainerTableLayoutPanel.Controls.Add(this.OperationResultDescriptionTextBox, 0, 1);
            this.OperationResultContainerTableLayoutPanel.Controls.Add(this.OKButton, 0, 2);
            this.OperationResultContainerTableLayoutPanel.Controls.Add(this.OperationResultLabel, 0, 0);
            this.OperationResultContainerTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OperationResultContainerTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.OperationResultContainerTableLayoutPanel.Name = "OperationResultContainerTableLayoutPanel";
            this.OperationResultContainerTableLayoutPanel.RowCount = 3;
            this.OperationResultContainerTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.OperationResultContainerTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.OperationResultContainerTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.OperationResultContainerTableLayoutPanel.Size = new System.Drawing.Size(600, 200);
            this.OperationResultContainerTableLayoutPanel.TabIndex = 0;
            // 
            // OperationResultDescriptionTextBox
            // 
            this.OperationResultDescriptionTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.OperationResultDescriptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OperationResultDescriptionTextBox.Location = new System.Drawing.Point(3, 16);
            this.OperationResultDescriptionTextBox.Multiline = true;
            this.OperationResultDescriptionTextBox.Name = "OperationResultDescriptionTextBox";
            this.OperationResultDescriptionTextBox.ReadOnly = true;
            this.OperationResultDescriptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.OperationResultDescriptionTextBox.Size = new System.Drawing.Size(594, 152);
            this.OperationResultDescriptionTextBox.TabIndex = 2;
            this.OperationResultDescriptionTextBox.TabStop = false;
            // 
            // OKButton
            // 
            this.OKButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.OKButton.Location = new System.Drawing.Point(522, 174);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 3;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // OperationResultLabel
            // 
            this.OperationResultLabel.AutoSize = true;
            this.OperationResultLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OperationResultLabel.Location = new System.Drawing.Point(3, 0);
            this.OperationResultLabel.Name = "OperationResultLabel";
            this.OperationResultLabel.Size = new System.Drawing.Size(594, 13);
            this.OperationResultLabel.TabIndex = 4;
            this.OperationResultLabel.Text = "[...]";
            // 
            // OperationResultHandler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 200);
            this.ControlBox = false;
            this.Controls.Add(this.OperationResultContainerTableLayoutPanel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OperationResultHandler";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Operation Result";
            this.TopMost = true;
            this.OperationResultContainerTableLayoutPanel.ResumeLayout(false);
            this.OperationResultContainerTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel OperationResultContainerTableLayoutPanel;
        private System.Windows.Forms.TextBox OperationResultDescriptionTextBox;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Label OperationResultLabel;
    }
}