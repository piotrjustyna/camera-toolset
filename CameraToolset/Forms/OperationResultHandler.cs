﻿using CameraToolset.Model;
using System;
using System.Windows.Forms;

namespace CameraToolset.Forms
{
    internal partial class OperationResultHandler : Form
    {
        private OperationResultHandler()
        {
            InitializeComponent();
        }

        private OperationResultHandler(OperationResult operationResult)
            : this()
        {
            OperationResultLabel.Text = operationResult.IsSuccess ? "Operation finished successfully." : "Operation failed.";
            OperationResultDescriptionTextBox.Text = operationResult.FailureDescription;
        }

        public static OperationResult HandleOperationResult(OperationResult operationResult)
        {
            if (!operationResult.IsSuccess)
            {
                using (OperationResultHandler handler = new OperationResultHandler(operationResult)) { handler.ShowDialog(); }
            }

            return operationResult;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
