﻿using System;

namespace CameraToolset.Model
{
    public static class CanonErrorDetector
    {
        public static OperationResult ProcessResultCode(UInt32 resultCode)
        {
            OperationResult result = new OperationResult(
                resultCode == EDSDKLib.EDSDK.EDS_ERR_OK
                , String.Format("0x{0:x16}", resultCode));

            return result;
        }
    }
}
