﻿using System;

namespace CameraToolset.Model
{
    public class OperationResult
    {
        public Boolean IsSuccess { get; private set; }

        public String FailureDescription { get; private set; }

        public OperationResult(
            Boolean isSuccess
            , String failureDescription)
        {
            IsSuccess = isSuccess;
            FailureDescription = failureDescription;
        }
    }
}
