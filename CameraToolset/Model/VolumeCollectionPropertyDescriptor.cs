﻿using System;
using System.ComponentModel;

namespace CameraToolset.Model
{
    public class VolumeCollectionPropertyDescriptor : PropertyDescriptor
    {
        private VolumeColleciton _volumes = null;
        private Int32 _index = -1;

        public VolumeCollectionPropertyDescriptor(
            VolumeColleciton volumes
            , Int32 index)
            : base("#" + index, null)
        {
            _volumes = volumes;
            _index = index;
        }

        public override AttributeCollection Attributes
        {
            get
            {
                return new AttributeCollection(null);
            }
        }

        public override Boolean CanResetValue(Object component)
        {
            return true;
        }

        public override Type ComponentType
        {
            get
            {
                return _volumes.GetType();
            }
        }

        public override Object GetValue(Object component)
        {
            return _volumes[_index];
        }

        public override Boolean IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public override string Name
        {
            get
            {
                return "#" + _index;
            }
        }

        public override Type PropertyType
        {
            get
            {
                return _volumes[_index].GetType();
            }
        }

        public override void ResetValue(Object component) { }

        public override void SetValue(Object component, Object value)
        {
            throw new NotImplementedException("This method is not implemented on purpose.");
        }

        public override Boolean ShouldSerializeValue(Object component)
        {
            return true;
        }

        public override String DisplayName
        {
            get
            {
                return String.Format(
                    "Volume {0}: {1}",
                    _index + 1,
                    _volumes[_index].Label);
            }
        }

        public override String Description
        {
            get
            {
                Volume volume = _volumes[_index];

                return String.Format(
                    "{0}, Free space: {1:f2}MB",
                    volume.StorageTypeDescription,
                    volume.FreeSpaceInMB);
            }
        }
    }
}
