﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace CameraToolset.Model
{
    public class ImageQualityConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(
            ITypeDescriptorContext context,
            CultureInfo culture,
            Object value,
            Type destinationType)
        {
            if (destinationType == typeof(String) &&
                value is ImageQuality)
            {
                ImageQuality quality = value as ImageQuality;

                return String.Format(
                    "Primary image: {0}, Secondary image: {1}",
                    quality.PrimaryImageSize,
                    quality.SecondaryImageSize);
            }

            return base.ConvertTo(
                context,
                culture,
                value,
                destinationType);
        }
    }
}
