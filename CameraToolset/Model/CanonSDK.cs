﻿using CameraToolset.Model.Retrievers;
using EDSDKLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace CameraToolset.Model
{
    public static class CanonSDK
    {
        public delegate void CameraPropertyChangedEventHandler(UInt32 propertyId);
        public delegate void DirectoryItemChangedEventHandler(IntPtr directoryItemReference);

        private static EDSDK.EdsObjectEventHandler _cameraToolsetObjectEventHandler = new EDSDK.EdsObjectEventHandler(CameraToolsetObjectEventHandler);
        private static EDSDK.EdsPropertyEventHandler _cameraToolsetPropertyEventHandler = new EDSDK.EdsPropertyEventHandler(CameraToolsetPropertyEventHandler);
        private static EDSDK.EdsStateEventHandler _cameraToolsetStateEventHandler = new EDSDK.EdsStateEventHandler(CameraToolsetStateEventHandler);
        private static CameraPropertyChangedEventHandler _cameraApertureChangedHandler = null;
        private static CameraPropertyChangedEventHandler _cameraIsoChangedHandler = null;
        private static CameraPropertyChangedEventHandler _cameraLiveViewOutputDeviceChangedHandler = null;
        private static CameraPropertyChangedEventHandler _cameraImageQualityChangedHandler = null;
        private static CameraPropertyChangedEventHandler _cameraJpegQualityChangedHandler = null;
        private static DirectoryItemChangedEventHandler _directoryItemChangedHandler = null;
        private static CameraApertureRetriever _cameraApertureRetriever = new CameraApertureRetriever();
        private static CameraIsoRetriever _cameraIsoRetriever = new CameraIsoRetriever();
        private static CameraLiveViewOutputDeviceRetriever _cameraLiveViewOutputDeviceRetriever = new CameraLiveViewOutputDeviceRetriever();
        private static CameraImageQualityRetriever _cameraImageQualityRetriever = new CameraImageQualityRetriever();
        private static CameraJpegQualityRetriever _cameraJpegQualityRetriever = new CameraJpegQualityRetriever();

        public static OperationResult Initialise()
        {
            return CanonErrorDetector.ProcessResultCode(EDSDK.EdsInitializeSDK());
        }

        public static OperationResult GetCameraListAndAssignEventHandlers(
            out List<Camera> cameras,
            CameraPropertyChangedEventHandler cameraApertureChangedHandler,
            CameraPropertyChangedEventHandler cameraIsoChangedHandler,
            CameraPropertyChangedEventHandler cameraOutputDeviceChangedHandler,
            CameraPropertyChangedEventHandler cameraImageQualityChangedHandler,
            CameraPropertyChangedEventHandler cameraJpegQualityChangedHandler,
            DirectoryItemChangedEventHandler directoryItemChangedHandler)
        {
            cameras = new List<Camera>();

            _cameraApertureChangedHandler = cameraApertureChangedHandler;
            _cameraIsoChangedHandler = cameraIsoChangedHandler;
            _cameraLiveViewOutputDeviceChangedHandler = cameraOutputDeviceChangedHandler;
            _cameraImageQualityChangedHandler = cameraImageQualityChangedHandler;
            _cameraJpegQualityChangedHandler = cameraJpegQualityChangedHandler;
            _directoryItemChangedHandler = directoryItemChangedHandler;

            IntPtr listOfCameras = IntPtr.Zero;

            OperationResult finalResult = null;
            OperationResult cameraListResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetCameraList(out listOfCameras));

            if (cameraListResult.IsSuccess)
            {
                Int32 numberOfCameras = -1;

                OperationResult countingCamerasResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetChildCount(listOfCameras, out numberOfCameras));

                if (countingCamerasResult.IsSuccess)
                {
                    if (numberOfCameras > 0)
                    {
                        OperationResult singleCameraResult = null;
                        OperationResult singleCameraDeviceInfoResult = null;
                        OperationResult eventHandlersResult = null;

                        for (Int32 i = 0; 
                            i < numberOfCameras && (eventHandlersResult == null || eventHandlersResult.IsSuccess);
                            i++)
                        {
                            IntPtr singleCameraReference = IntPtr.Zero;
                            Camera newCamera = null;
                            singleCameraResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetChildAtIndex(listOfCameras, i, out singleCameraReference));

                            if (singleCameraResult.IsSuccess
                                && singleCameraReference != IntPtr.Zero)
                            {
                                EDSDK.EdsDeviceInfo deviceInfo;

                                singleCameraDeviceInfoResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetDeviceInfo(singleCameraReference, out deviceInfo));

                                if (singleCameraDeviceInfoResult.IsSuccess)
                                {
                                    newCamera = new Camera(deviceInfo.szDeviceDescription, singleCameraReference);
                                }
                                else
                                {
                                    newCamera = new Camera("Unidentified", singleCameraReference);
                                }
                            }
                            else
                            {
                                newCamera = new Camera("Unidentified", singleCameraReference);
                            }

                            cameras.Add(newCamera);
                        }

                        finalResult = new OperationResult(true, cameras.Count.ToString());
                    }
                    else
                    {
                        finalResult = new OperationResult(false, "No cameras found.");
                    }
                }
                else
                {
                    finalResult = countingCamerasResult;
                }
            }
            else
            {
                finalResult = cameraListResult;
            }

            return finalResult;
        }

        public static OperationResult Terminate()
        {
            return CanonErrorDetector.ProcessResultCode(EDSDK.EdsTerminateSDK());
        }

        public static OperationResult RetrieveCameraDetails(Camera selectedCamera)
        {
            Int32 numberOfCameraVolumes = -1;

            OperationResult finalResult = null;
            OperationResult numberOfCameraVolumesResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetChildCount(selectedCamera.Reference, out numberOfCameraVolumes));

            if (numberOfCameraVolumesResult.IsSuccess
                && numberOfCameraVolumes > 0)
            {
                IntPtr singleVolume = IntPtr.Zero;
                OperationResult volumeReferenceResult = null;

                for (Int32 volumeIndex = 0;
                    volumeIndex < numberOfCameraVolumes && (volumeReferenceResult == null || volumeReferenceResult.IsSuccess);
                    volumeIndex++)
                {
                    volumeReferenceResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetChildAtIndex(selectedCamera.Reference, volumeIndex, out singleVolume));

                    if (volumeReferenceResult.IsSuccess)
                    {
                        EDSDK.EdsVolumeInfo singleVolumeInfo;

                        OperationResult singleVolumeInfoResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetVolumeInfo(singleVolume, out singleVolumeInfo));

                        if (singleVolumeInfoResult.IsSuccess)
                        {
                            selectedCamera.Volumes.Add(
                                new Volume(
                                    singleVolumeInfo.szVolumeLabel,
                                    singleVolumeInfo.Access,
                                    singleVolumeInfo.FreeSpaceInBytes,
                                    singleVolumeInfo.StorageType));

                            finalResult = new OperationResult(true, null);
                        }
                        else
                        {
                            // could not retrieve volume's details
                            finalResult = singleVolumeInfoResult;
                        }
                    }
                    else
                    {
                        // could not detect volume's reference
                        finalResult = volumeReferenceResult;
                    }
                }
            }
            else
            {
                // no volumes detected
                finalResult = numberOfCameraVolumesResult;
            }

            return finalResult;
        }

        public static OperationResult OpenCameraSession(Camera selectedCamera)
        {
            var eventHandlersResult = AssignEventHandlers(selectedCamera);

            if (eventHandlersResult.IsSuccess)
            {
                eventHandlersResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsOpenSession(selectedCamera.Reference));
            }
            else
            {
                // event handlers could not be assigned, so session should not be opened
            }

            return eventHandlersResult;
        }

        public static OperationResult CloseCameraSession(Camera selectedCamera)
        {
            // result of removing the event handlers is not really needed to close the session
            RemoveEventHandlers(selectedCamera);

            return CanonErrorDetector.ProcessResultCode(EDSDK.EdsCloseSession(selectedCamera.Reference));
        }

        public static String GetCameraAperture(Camera currentCamera, UInt32 propertyId)
        {
            return _cameraApertureRetriever.GetCameraAperture(currentCamera, propertyId);
        }

        public static String GetCameraIso(Camera currentCamera, UInt32 propertyId)
        {
            return _cameraIsoRetriever.GetCameraIso(currentCamera, propertyId);
        }

        public static String GetCameraLiveViewOutputDevice(Camera currentCamera, UInt32 propertyId)
        {
            return _cameraLiveViewOutputDeviceRetriever.GetCameraLiveViewOutputDevice(currentCamera, propertyId);
        }

        public static ImageQuality GetCameraImageQuality(Camera currentCamera, UInt32 propertyId)
        {
            return _cameraImageQualityRetriever.GetCameraImageQuality(currentCamera, propertyId);
        }

        public static String GetCameraJpegQuality(Camera currentCamera, UInt32 propertyId)
        {
            return _cameraJpegQualityRetriever.GetCameraJpegQuality(currentCamera, propertyId);
        }

        public static Boolean IsChangingDirectoryItemAPicture(IntPtr directoryItemReference, out PictureDirectoryItemInfo directoryItemInfo)
        {
            Boolean result = false;
            EDSDK.EdsDirectoryItemInfo sdkDirectoryItemInfo;

            OperationResult dirItemInfoResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetDirectoryItemInfo(directoryItemReference, out sdkDirectoryItemInfo));

            if (dirItemInfoResult.IsSuccess)
            {
                if (sdkDirectoryItemInfo.format == (uint)EDSDK.EdsTargetImageType.Unknown)
                {
                    // folder or unknown directory item info
                    directoryItemInfo = null;
                }
                else
                {
                    directoryItemInfo = new PictureDirectoryItemInfo(
                            sdkDirectoryItemInfo.szFileName,
                            sdkDirectoryItemInfo.Size);

                    result = true;
                }
            }
            else
            {
                // something went wrong while recognising what the directory item is
                directoryItemInfo = null;
            }

            return result;
        }

        public static OperationResult DownloadPicture(IntPtr pictureReference, PictureDirectoryItemInfo pictureInfo, out Image picture)
        {
            IntPtr stream = IntPtr.Zero;
            String path = String.Concat(@".\", pictureInfo.Name);
            OperationResult result = null;
            String unsupportedFormat = "CR2";

            picture = null;

            if (path.EndsWith(String.Concat(".", unsupportedFormat), true, System.Globalization.CultureInfo.InvariantCulture))
            {
                result = new OperationResult(false, String.Format("Format \"{0}\" not supported.", unsupportedFormat));
            }
            else
            {
                OperationResult streamResult = CanonErrorDetector.ProcessResultCode(
                    EDSDK.EdsCreateFileStream(
                        path,
                        EDSDKLib.EDSDK.EdsFileCreateDisposition.CreateNew,
                        EDSDKLib.EDSDK.EdsAccess.ReadWrite,
                        out stream));

                if (streamResult.IsSuccess)
                {
                    OperationResult downloadResult = CanonErrorDetector.ProcessResultCode(
                        EDSDKLib.EDSDK.EdsDownload(
                        pictureReference,
                        pictureInfo.Size,
                        stream));

                    if (downloadResult.IsSuccess)
                    {
                        // great, we have the picture!
                    }
                    else
                    {
                        // picture could not be downloaded
                    }

                    result = downloadResult;

                    OperationResult downloadCompleteNotificationResult = CanonErrorDetector.ProcessResultCode(
                        EDSDKLib.EDSDK.EdsDownloadComplete(pictureReference));

                    if (downloadCompleteNotificationResult.IsSuccess)
                    {
                        // "download complete" notification sent
                    }
                    else
                    {
                        // camera could not be notified that the download is complete
                    }

                    result = downloadCompleteNotificationResult;

                    streamResult = CanonErrorDetector.ProcessResultCode(
                        EDSDKLib.EDSDK.EdsRelease(stream));

                    if (streamResult.IsSuccess)
                    {
                        // stream properly closed
                    }
                    else
                    {
                        // stream could not be closed
                    }

                    result = streamResult;
                }
                else
                {
                    // file stream could not be opened
                    result = streamResult;
                }

                if (result.IsSuccess)
                {
                    picture = Image.FromFile(path);
                }
                else
                {
                    // could not retrieve the picture because of a combination of errors
                }
            }

            return result;
        }

        public static OperationResult SetCameraLiveViewOutputDeviceToPC(Camera currentCamera)
        {
            return CanonErrorDetector.ProcessResultCode(
                EDSDK.EdsSetPropertyData(
                    currentCamera.Reference,
                    EDSDK.PropID_Evf_OutputDevice,
                    0,
                    sizeof(UInt32),
                    EDSDK.EvfOutputDevice_PC));
        }

        public static OperationResult SetCameraLiveViewOutputDeviceToNone(Camera currentCamera)
        {
            return CanonErrorDetector.ProcessResultCode(
                    EDSDK.EdsSetPropertyData(
                        currentCamera.Reference,
                        EDSDK.PropID_Evf_OutputDevice,
                        0,
                        sizeof(UInt32),
                        EDSDK.EvfOutputDevice_None));
        }

        public static OperationResult SendTakePictureCommand(Camera currentCamera)
        {
            return CanonErrorDetector.ProcessResultCode(EDSDK.EdsSendCommand(currentCamera.Reference, EDSDK.CameraCommand_TakePicture, 0));
        }

        private static OperationResult AssignEventHandlers(Camera camera)
        {
            var eventHandlerResult = CanonErrorDetector.ProcessResultCode(
                EDSDK.EdsSetObjectEventHandler(
                    camera.Reference,
                    EDSDK.ObjectEvent_All,
                    _cameraToolsetObjectEventHandler,
                    IntPtr.Zero));

            if (eventHandlerResult.IsSuccess)
            {
                eventHandlerResult = CanonErrorDetector.ProcessResultCode(
                    EDSDK.EdsSetPropertyEventHandler(
                        camera.Reference,
                        EDSDK.PropertyEvent_All,
                        _cameraToolsetPropertyEventHandler,
                        IntPtr.Zero));

                if (eventHandlerResult.IsSuccess)
                {
                    eventHandlerResult = CanonErrorDetector.ProcessResultCode(
                        EDSDK.EdsSetCameraStateEventHandler(
                            camera.Reference,
                            EDSDK.StateEvent_All,
                            _cameraToolsetStateEventHandler,
                            IntPtr.Zero));

                    if (eventHandlerResult.IsSuccess)
                    {
                        // no plans for this scenario right now
                    }
                    else
                    {
                        // camera state event handler could not be assigned
                    }
                }
                else
                {
                    // property event handler could not be assigned
                }
            }
            else
            {
                // object event handler could not be assigned
            }

            return eventHandlerResult;
        }

        private static OperationResult RemoveEventHandlers(Camera camera)
        {
            var eventHandlerResult = CanonErrorDetector.ProcessResultCode(
                EDSDK.EdsSetObjectEventHandler(
                    camera.Reference,
                    EDSDK.ObjectEvent_All,
                    null,
                    IntPtr.Zero));

            if (eventHandlerResult.IsSuccess)
            {
                eventHandlerResult = CanonErrorDetector.ProcessResultCode(
                    EDSDK.EdsSetPropertyEventHandler(
                        camera.Reference,
                        EDSDK.PropertyEvent_All,
                        null,
                        IntPtr.Zero));

                if (eventHandlerResult.IsSuccess)
                {
                    eventHandlerResult = CanonErrorDetector.ProcessResultCode(
                        EDSDK.EdsSetCameraStateEventHandler(
                            camera.Reference,
                            EDSDK.StateEvent_All,
                            null,
                            IntPtr.Zero));

                    if (eventHandlerResult.IsSuccess)
                    {
                        // no plans for this scenario right now
                    }
                    else
                    {
                        // camera state event handler could not be assigned
                    }
                }
                else
                {
                    // property event handler could not be assigned
                }
            }
            else
            {
                // object event handler could not be assigned
            }

            return eventHandlerResult;
        }

        private static UInt32 CameraToolsetObjectEventHandler(UInt32 inEvent, IntPtr inRef, IntPtr inContext)
        {
            if (inEvent == EDSDK.ObjectEvent_VolumeInfoChanged)
            {
                // TODO: implement volume info changing notification
                Console.WriteLine("Volume object state has been changed. Volume object reference: " + inRef + ".");

                EDSDK.EdsVolumeInfo volumeInfo;

                OperationResult volumeInfoResult = CanonErrorDetector.ProcessResultCode(EDSDK.EdsGetVolumeInfo(inRef, out volumeInfo));
                // - TODO: implement volume info changing notification
            }
            else if (inEvent == EDSDK.ObjectEvent_DirItemCreated)
            {
                _directoryItemChangedHandler(inRef);
            }
            else
            {
                // unrecognised state - ignoring the event
            }

            return 0;
        }

        private static UInt32 CameraToolsetPropertyEventHandler(UInt32 inEvent, UInt32 inPropertyId, UInt32 inParam, IntPtr inContext)
        {
            if (inPropertyId == EDSDK.PropID_Av)
            {
                _cameraApertureChangedHandler(inPropertyId);
            }
            else if (inPropertyId == EDSDK.PropID_ISOSpeed)
            {
                _cameraIsoChangedHandler(inPropertyId);
            }
            else if (inPropertyId == EDSDK.PropID_Evf_OutputDevice)
            {
                _cameraLiveViewOutputDeviceChangedHandler(inPropertyId);
            }
            else if (inPropertyId == EDSDK.PropID_ImageQuality)
            {
                _cameraImageQualityChangedHandler(inPropertyId);
            }
            else if (inPropertyId == EDSDK.PropID_JpegQuality)
            {
                _cameraJpegQualityChangedHandler(inPropertyId);
            }
            else
            {
                // unrecognised property - ignoring the event
            }

            return 0;
        }

        private static UInt32 CameraToolsetStateEventHandler(UInt32 inEvent, UInt32 inParameter, IntPtr inContext)
        {
            if (inEvent == EDSDK.StateEvent_JobStatusChanged)
            {
                Console.WriteLine("There are {0}objects to be transferred to a host computer.", inParameter == 0 ? "no " : String.Empty);
            }
            else
            {
                // unrecognised state - ignoring the event
            }

            return 0;
        }
    }
}
