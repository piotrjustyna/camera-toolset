﻿using System;

namespace CameraToolset.Model
{
    public static class StorageTypeConverter
    {
        public static String ConvertStorageType(UInt32 storageType)
        {
            String result = String.Empty;

            if (storageType == 0)
            {
                result = "No memory card inserted";
            }
            else if (storageType == 1)
            {
                result = "Compact flash";
            }
            else if (storageType == 2)
            {
                result = "SD card";
            }
            else
            {
                result = "Storage Error";
            }

            return result;
        }
    }
}
