﻿using System.ComponentModel;
using System.Drawing.Design;

namespace CameraToolset.Model
{
    public class VolumeCollectionEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.None;
        }
    }
}
