﻿using System;
using System.ComponentModel;

namespace CameraToolset.Model
{
    public class Camera
    {
        public Camera(
            String name,
            IntPtr reference)
        {
            Name = name;
            Reference = reference;
            Volumes = new VolumeColleciton();
        }

        [Category("Camera")]
        [DisplayName("Name")]
        [Description("Camera's name.")]
        public String Name { get; private set; }

        [Category("Camera")]
        [DisplayName("Aperture")]
        [Description("Camera's aperture.")]
        [ReadOnly(true)]
        public String Aperture { get; set; }

        [Category("Camera")]
        [DisplayName("ISO")]
        [Description("Camera's ISO speed.")]
        [ReadOnly(true)]
        public String Iso { get; set; }

        [Category("Camera")]
        [DisplayName("Live view output device")]
        [Description("Camera's Live view output device.")]
        [ReadOnly(true)]
        public String LiveViewOutputDevice { get; set; }

        [Category("Camera")]
        [DisplayName("Image quality")]
        [Description("Camera's Image quality.")]
        [ReadOnly(true)]
        public ImageQuality ImageQuality { get; set; }

        [Category("Camera")]
        [DisplayName("JPEG quality")]
        [Description("Camera's JPEG quality.")]
        [ReadOnly(true)]
        public String JpegQuality { get; set; }

        [Browsable(false)]
        public IntPtr Reference { get; private set; }

        [Category("Camera")]
        [DisplayName("Volumes")]
        [Description("List of camera's volumes.")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public VolumeColleciton Volumes { get; private set; }
    }
}
