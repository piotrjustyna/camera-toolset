﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace CameraToolset.Model
{
    public class SingleConverter : TypeConverter
    {
        public override bool CanConvertFrom(
            ITypeDescriptorContext context, 
            Type sourceType)
        {
            return sourceType == typeof(Single);
        }

        public override object ConvertTo(
            ITypeDescriptorContext context, 
            CultureInfo culture, 
            Object value, 
            Type destinationType)
        {
            if (destinationType == typeof(String))
            {
                return ((Single)value).ToString("f2");
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
