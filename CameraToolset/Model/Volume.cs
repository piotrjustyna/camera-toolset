﻿using System;
using System.ComponentModel;

namespace CameraToolset.Model
{
    [TypeConverter(typeof(VolumeConverter))]
    public class Volume
    {
        public Volume(
            String label
            , UInt32 accessType
            , UInt64 freeSpaceInBytes
            , UInt32 storageType)
        {
            Label = label;
            AccessTypeDescription = AccessTypeConverter.ConvertAccessType(accessType);
            FreeSpaceInMB = freeSpaceInBytes / (Single)1024.0;
            StorageTypeDescription = StorageTypeConverter.ConvertStorageType(storageType);
        }

        [DisplayName("Label")]
        [Description("Volume's label.")]
        public String Label { get; private set; }

        [DisplayName("Access")]
        [Description("Type of Access.")]
        public String AccessTypeDescription { get; private set; }

        [DisplayName("Free space [MB]")]
        [Description("Available space.")]
        [TypeConverter(typeof(SingleConverter))]
        public Single FreeSpaceInMB { get; private set; }

        [DisplayName("Storage")]
        [Description("Type of storage.")]
        public String StorageTypeDescription { get; private set; }
    }
}
