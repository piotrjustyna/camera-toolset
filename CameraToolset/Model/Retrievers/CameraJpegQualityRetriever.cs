﻿using EDSDKLib;
using System;

namespace CameraToolset.Model.Retrievers
{
    public class CameraJpegQualityRetriever
    {
        public String GetCameraJpegQuality(Camera currentCamera, UInt32 inPropertyID)
        {
            UInt32 propertyIntegerData;

            EDSDK.EdsGetPropertyData(
                currentCamera.Reference,
                inPropertyID,
                0,
                out propertyIntegerData);

            return propertyIntegerData.ToString();
        }
    }
}
