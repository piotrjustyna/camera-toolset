﻿using EDSDKLib;
using System;

namespace CameraToolset.Model.Retrievers
{
    public class CameraApertureRetriever
    {
        public String GetCameraAperture(Camera currentCamera, UInt32 inPropertyID)
        {
            String aperture = null;
            UInt32 propertyIntegerData;

            EDSDK.EdsGetPropertyData(
                currentCamera.Reference,
                inPropertyID,
                0,
                out propertyIntegerData);

            if (propertyIntegerData == 0x08)
            {
                aperture = "1";
            }
            else if (propertyIntegerData == 0x0B)
            {
                aperture = "1.1";
            }
            else if (propertyIntegerData == 0x0C)
            {
                aperture = "1.2";
            }
            else if (propertyIntegerData == 0x0D)
            {
                aperture = "1.2 (1/3)";
            }
            else if (propertyIntegerData == 0x10)
            {
                aperture = "1.4";
            }
            else if (propertyIntegerData == 0x13)
            {
                aperture = "1.6";
            }
            else if (propertyIntegerData == 0x14)
            {
                aperture = "1.8";
            }
            else if (propertyIntegerData == 0x15)
            {
                aperture = "1.8 (1/3)";
            }
            else if (propertyIntegerData == 0x18)
            {
                aperture = "2";
            }
            else if (propertyIntegerData == 0x1B)
            {
                aperture = "2.2";
            }
            else if (propertyIntegerData == 0x1C)
            {
                aperture = "2.5";
            }
            else if (propertyIntegerData == 0x1D)
            {
                aperture = "2.5 (1/3)";
            }
            else if (propertyIntegerData == 0x20)
            {
                aperture = "2.8";
            }
            else if (propertyIntegerData == 0x23)
            {
                aperture = "3.2";
            }
            else if (propertyIntegerData == 0x24)
            {
                aperture = "3.5";
            }
            else if (propertyIntegerData == 0x25)
            {
                aperture = "3.5 (1/3)";
            }
            else if (propertyIntegerData == 0x28)
            {
                aperture = "4";
            }
            else if (propertyIntegerData == 0x2B)
            {
                aperture = "4.5";
            }
            else if (propertyIntegerData == 0x2C)
            {
                aperture = "4.5";
            }
            else if (propertyIntegerData == 0x2D)
            {
                aperture = "5.0";
            }
            else if (propertyIntegerData == 0x30)
            {
                aperture = "5.6";
            }
            else if (propertyIntegerData == 0x33)
            {
                aperture = "6.3";
            }
            else if (propertyIntegerData == 0x34)
            {
                aperture = "6.7";
            }
            else if (propertyIntegerData == 0x35)
            {
                aperture = "7.1";
            }
            else if (propertyIntegerData == 0x38)
            {
                aperture = "8";
            }
            else if (propertyIntegerData == 0x3B)
            {
                aperture = "9";
            }
            else if (propertyIntegerData == 0x3C)
            {
                aperture = "9.5";
            }
            else if (propertyIntegerData == 0x3D)
            {
                aperture = "10";
            }
            else if (propertyIntegerData == 0x40)
            {
                aperture = "11";
            }
            else if (propertyIntegerData == 0x43)
            {
                aperture = "13 (1/3)";
            }
            else if (propertyIntegerData == 0x44)
            {
                aperture = "13";
            }
            else if (propertyIntegerData == 0x45)
            {
                aperture = "14";
            }
            else if (propertyIntegerData == 0x48)
            {
                aperture = "16";
            }
            else if (propertyIntegerData == 0x4B)
            {
                aperture = "18";
            }
            else if (propertyIntegerData == 0x4C)
            {
                aperture = "19";
            }
            else if (propertyIntegerData == 0x4D)
            {
                aperture = "20";
            }
            else if (propertyIntegerData == 0x50)
            {
                aperture = "22";
            }
            else if (propertyIntegerData == 0x53)
            {
                aperture = "25";
            }
            else if (propertyIntegerData == 0x54)
            {
                aperture = "27";
            }
            else if (propertyIntegerData == 0x55)
            {
                aperture = "29";
            }
            else if (propertyIntegerData == 0x58)
            {
                aperture = "32";
            }
            else if (propertyIntegerData == 0x5B)
            {
                aperture = "36";
            }
            else if (propertyIntegerData == 0x5C)
            {
                aperture = "38";
            }
            else if (propertyIntegerData == 0x5D)
            {
                aperture = "40";
            }
            else if (propertyIntegerData == 0x60)
            {
                aperture = "45";
            }
            else if (propertyIntegerData == 0x63)
            {
                aperture = "51";
            }
            else if (propertyIntegerData == 0x64)
            {
                aperture = "54";
            }
            else if (propertyIntegerData == 0x65)
            {
                aperture = "57";
            }
            else if (propertyIntegerData == 0x68)
            {
                aperture = "64";
            }
            else if (propertyIntegerData == 0x6B)
            {
                aperture = "72";
            }
            else if (propertyIntegerData == 0x6C)
            {
                aperture = "76";
            }
            else if (propertyIntegerData == 0x6D)
            {
                aperture = "80";
            }
            else if (propertyIntegerData == 0x70)
            {
                aperture = "91";
            }
            else if (propertyIntegerData == 0xffffffff)
            {
                aperture = "invalid";
            }
            else
            {
                aperture = "unknown";
            }

            return "f/" + aperture;
        }
    }
}
