﻿using EDSDKLib;
using System;

namespace CameraToolset.Model.Retrievers
{
    public class CameraIsoRetriever
    {
        public String GetCameraIso(Camera currentCamera, UInt32 inPropertyID)
        {
            String iso = null;
            UInt32 propertyIntegerData;

            EDSDK.EdsGetPropertyData(
                currentCamera.Reference,
                inPropertyID,
                0,
                out propertyIntegerData);

            if (propertyIntegerData == 0x00)
            {
                iso = "Auto";
            }
            else if (propertyIntegerData == 0x40)
            {
                iso = "50";
            }
            else if (propertyIntegerData == 0x48)
            {
                iso = "100";
            }
            else if (propertyIntegerData == 0x4B)
            {
                iso = "125";
            }
            else if (propertyIntegerData == 0x4D)
            {
                iso = "160";
            }
            else if (propertyIntegerData == 0x50)
            {
                iso = "200";
            }
            else if (propertyIntegerData == 0x53)
            {
                iso = "250";
            }
            else if (propertyIntegerData == 0x55)
            {
                iso = "320";
            }
            else if (propertyIntegerData == 0x58)
            {
                iso = "400";
            }
            else if (propertyIntegerData == 0x5B)
            {
                iso = "500";
            }
            else if (propertyIntegerData == 0x5D)
            {
                iso = "640";
            }
            else if (propertyIntegerData == 0x60)
            {
                iso = "800";
            }
            else if (propertyIntegerData == 0x63)
            {
                iso = "1000";
            }
            else if (propertyIntegerData == 0x65)
            {
                iso = "1250";
            }
            else if (propertyIntegerData == 0x68)
            {
                iso = "1600";
            }
            else if (propertyIntegerData == 0x6B)
            {
                iso = "2000";
            }
            else if (propertyIntegerData == 0x6D)
            {
                iso = "2500";
            }
            else if (propertyIntegerData == 0x70)
            {
                iso = "3200";
            }
            else if (propertyIntegerData == 0x73)
            {
                iso = "4000";
            }
            else if (propertyIntegerData == 0x75)
            {
                iso = "5000";
            }
            else if (propertyIntegerData == 0x78)
            {
                iso = "6400";
            }
            else if (propertyIntegerData == 0x7B)
            {
                iso = "8000";
            }
            else if (propertyIntegerData == 0x7D)
            {
                iso = "10000";
            }
            else if (propertyIntegerData == 0x80)
            {
                iso = "12800";
            }
            else if (propertyIntegerData == 0x88)
            {
                iso = "25600";
            }
            else if (propertyIntegerData == 0x90)
            {
                iso = "51200";
            }
            else if (propertyIntegerData == 0x98)
            {
                iso = "102400";
            }
            else if (propertyIntegerData == 0xffffffff)
            {
                iso = "invalid";
            }
            else
            {
                iso = "unknown";
            }

            return iso;
        }
    }
}
