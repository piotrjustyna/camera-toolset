﻿using EDSDKLib;
using System;

namespace CameraToolset.Model.Retrievers
{
    public class CameraLiveViewOutputDeviceRetriever
    {
        public String GetCameraLiveViewOutputDevice(Camera currentCamera, UInt32 propertyId)
        {
            String outputDevice = null;
            UInt32 propertyIntegerData;

            EDSDK.EdsGetPropertyData(
                currentCamera.Reference,
                propertyId,
                0,
                out propertyIntegerData);

            if (propertyIntegerData == 0x00)
            {
                outputDevice = "not in live view";
            }
            else if (propertyIntegerData == 0x01)
            {
                outputDevice = "Camera";
            }
            else if (propertyIntegerData == 0x02)
            {
                outputDevice = "PC";
            }
            else
            {
                outputDevice = "unknown";
            }

            return outputDevice;
        }
    }
}
