﻿using EDSDKLib;
using System;

namespace CameraToolset.Model.Retrievers
{
    public class CameraImageQualityRetriever
    {
        public ImageQuality GetCameraImageQuality(Camera currentCamera, UInt32 propertyId)
        {
            UInt32 propertyIntegerData;

            EDSDK.EdsGetPropertyData(
                currentCamera.Reference,
                propertyId,
                0,
                out propertyIntegerData);

            Func<UInt32, String> getImageSizeByIdentifier = (sizeIdentifier) =>
                {
                    String imageSize = null;

                    if (sizeIdentifier == EDSDK.ImageSize_Large)
                    {
                        imageSize = "Large";
                    }
                    else if (sizeIdentifier == EDSDK.ImageSize_Middle)
                    {
                        imageSize = "Medium";
                    }
                    else if (sizeIdentifier == EDSDK.ImageSize_Middle1)
                    {
                        imageSize = "Medium 1";
                    }
                    else if (sizeIdentifier == EDSDK.ImageSize_Middle2)
                    {
                        imageSize = "Medium 2";
                    }
                    else if (sizeIdentifier == EDSDK.ImageSize_Small)
                    {
                        imageSize = "Small";
                    }
                    else
                    {
                        imageSize = "unknown";
                    }

                    return imageSize;
                };

            Func<UInt32, String> getImageFormatByIdentifier = (formatIdentifier) =>
                {
                    String imageFormat = null;

                    if (formatIdentifier == EDSDK.ImageFormat_Jpeg)
                    {
                        imageFormat = "JPEG";
                    }
                    else if (formatIdentifier == EDSDK.ImageFormat_CRW)
                    {
                        imageFormat = "CRW";
                    }
                    else if (formatIdentifier == EDSDK.ImageFormat_RAW)
                    {
                        imageFormat = "RAW";
                    }
                    else if (formatIdentifier == EDSDK.ImageFormat_CR2)
                    {
                        imageFormat = "CR2";
                    }
                    else
                    {
                        imageFormat = "unknown";
                    }

                    return imageFormat;
                };

            Func<UInt32, String> getImageCompressionQualityByIdentifier = (compressionQualityIdentifier) =>
                {
                    String imageCompressionQuality = null;

                    if (compressionQualityIdentifier == EDSDK.CompressQuality_Normal)
                    {
                        imageCompressionQuality = "Normal";
                    }
                    else if (compressionQualityIdentifier == EDSDK.CompressQuality_Fine)
                    {
                        imageCompressionQuality = "Fine";
                    }
                    else if (compressionQualityIdentifier == EDSDK.CompressQuality_Lossless)
                    {
                        imageCompressionQuality = "Lossless";
                    }
                    else if (compressionQualityIdentifier == EDSDK.CompressQuality_SuperFine)
                    {
                        imageCompressionQuality = "SuperFine";
                    }
                    else
                    {
                        imageCompressionQuality = "unknown";
                    }

                    return imageCompressionQuality;
                };

            String binaryGroupsFormat = "{0}{1}{2}{3}{4}{5}{6}{7}";
            String ones = "1111";
            String zeros = "0000";

            UInt32 primaryImageSizeMask = Convert.ToUInt32(String.Format(binaryGroupsFormat, ones, ones, zeros, zeros, zeros, zeros, zeros, zeros), 2);
            UInt32 primaryImageFormatMask = Convert.ToUInt32(String.Format(binaryGroupsFormat, zeros, zeros, ones, zeros, zeros, zeros, zeros, zeros), 2);
            UInt32 primaryImageCompressionQualityMask = Convert.ToUInt32(String.Format(binaryGroupsFormat, zeros, zeros, zeros, ones, zeros, zeros, zeros, zeros), 2);
            UInt32 secondaryImageSizeMask = Convert.ToUInt32(String.Format(binaryGroupsFormat, zeros, zeros, zeros, zeros, zeros, ones, zeros, zeros), 2);
            UInt32 secondaryImageFormatMask = Convert.ToUInt32(String.Format(binaryGroupsFormat, zeros, zeros, zeros, zeros, zeros, zeros, ones, zeros), 2);
            UInt32 secondaryImageCompressionQualityMask = Convert.ToUInt32(String.Format(binaryGroupsFormat, zeros, zeros, zeros, zeros, zeros, zeros, zeros, ones), 2);
            
            UInt32 primaryImageSizeIdentifier = (propertyIntegerData & primaryImageSizeMask) >> 24;
            UInt32 primaryImageFormatIdentifier = (propertyIntegerData & primaryImageFormatMask) >> 20;
            UInt32 primaryImageCompressionQualityIdentifier = (propertyIntegerData & primaryImageCompressionQualityMask) >> 16;
            UInt32 secondaryImageSizeIdentifier = (propertyIntegerData & secondaryImageSizeMask) >> 8;
            UInt32 secondaryImageFormatIdentifier = (propertyIntegerData & secondaryImageFormatMask) >> 4;
            UInt32 secondaryImageCompressionQualityIdentifier = (propertyIntegerData & secondaryImageCompressionQualityMask);

            String primaryImageSize = getImageSizeByIdentifier(primaryImageSizeIdentifier);
            String primaryImageFormat = getImageFormatByIdentifier(primaryImageFormatIdentifier);
            String primarymageCompressionQuality = getImageCompressionQualityByIdentifier(primaryImageCompressionQualityIdentifier);
            String secondaryImageSize = getImageSizeByIdentifier(secondaryImageSizeIdentifier);
            String secondaryImageFormat = getImageFormatByIdentifier(secondaryImageFormatIdentifier);
            String secondaryImageCompressionQuality = getImageCompressionQualityByIdentifier(secondaryImageCompressionQualityIdentifier);

            return new ImageQuality
            {
                PrimaryImageSize = primaryImageSize,
                PrimaryImageFormat = primaryImageFormat,
                PrimaryImageCompressionQuality = primarymageCompressionQuality,
                SecondaryImageSize = secondaryImageSize,
                SecondaryImageFormat = secondaryImageFormat,
                SecondaryImageCompressionQuality = secondaryImageCompressionQuality
            };
        }
    }
}
