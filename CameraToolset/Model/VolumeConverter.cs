﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace CameraToolset.Model
{
    public class VolumeConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(
            ITypeDescriptorContext context,
            CultureInfo culture,
            Object value,
            Type destinationType)
        {
            if (destinationType == typeof(String) &&
                value is Volume)
            {
                Volume volume = value as Volume;

                return String.Format(
                    "{0}, Free space: {1}MB",
                    volume.StorageTypeDescription,
                    volume.FreeSpaceInMB.ToString("f2"));
            }

            return base.ConvertTo(
                context,
                culture,
                value,
                destinationType);
        }
    }
}
