﻿using System;
using System.ComponentModel;

namespace CameraToolset.Model
{
    [TypeConverter(typeof(ImageQualityConverter))]
    public class ImageQuality
    {
        [DisplayName("Primary image size")]
        [Description("Primary image size.")]
        [ReadOnly(true)]
        public String PrimaryImageSize { get; set; }

        [DisplayName("Primary image format")]
        [Description("Primary image format.")]
        [ReadOnly(true)]
        public String PrimaryImageFormat { get; set; }

        [DisplayName("Primary image compression quality")]
        [Description("Primary image compression quality.")]
        [ReadOnly(true)]
        public String PrimaryImageCompressionQuality { get; set; }

        [DisplayName("Secondary image size")]
        [Description("Secondary image size.")]
        [ReadOnly(true)]
        public String SecondaryImageSize { get; set; }

        [DisplayName("Secondary image format")]
        [Description("Secondary image format.")]
        [ReadOnly(true)]
        public String SecondaryImageFormat { get; set; }

        [DisplayName("Secondary image compression quality")]
        [Description("Secondary image compression quality.")]
        [ReadOnly(true)]
        public String SecondaryImageCompressionQuality { get; set; }
    }
}
