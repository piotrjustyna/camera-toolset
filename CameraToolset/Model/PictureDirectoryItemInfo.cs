﻿using System;

namespace CameraToolset.Model
{
    public class PictureDirectoryItemInfo
    {
        public String Name { get; private set; }

        public UInt32 Size { get; private set; }

        public PictureDirectoryItemInfo(
            String name
            , UInt32 size)
        {
            Name = name;
            Size = size;
        }
    }
}
