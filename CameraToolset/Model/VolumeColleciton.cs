﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;

namespace CameraToolset.Model
{
    [Editor(typeof(VolumeCollectionEditor), typeof(UITypeEditor))]
    public class VolumeColleciton : CollectionBase, ICustomTypeDescriptor
    {
        public void Add(Volume volume)
        {
            List.Add(volume);
        }

        public Volume this[Int32 index]
        {
            get
            {
                return List[index] as Volume;
            }
        }

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public String GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public String GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public Object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        public Object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return GetProperties();
        }

        public PropertyDescriptorCollection GetProperties()
        {
            var propertyDescriptorCollection = new PropertyDescriptorCollection(null);

            for (Int32 i = 0; i < List.Count; i++)
            {
                propertyDescriptorCollection.Add(new VolumeCollectionPropertyDescriptor(this, i));
            }

            return propertyDescriptorCollection;
        }

        public override string ToString()
        {
            return String.Empty;
        }
    }
}
