﻿using System;

namespace CameraToolset.Model
{
    public static class AccessTypeConverter
    {
        public static String ConvertAccessType(UInt32 accessType)
        {
            String result = String.Empty;

            if (accessType == 0)
            {
                result = "Read Only";
            }
            else if (accessType == 1)
            {
                result = "Write Only";
            }
            else if (accessType == 2)
            {
                result = "Read and Write";
            }
            else
            {
                result = "Access Error";
            }

            return result;
        }
    }
}
