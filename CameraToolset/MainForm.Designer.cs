﻿namespace CameraToolset
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ControlPanelTabControl = new System.Windows.Forms.TabControl();
            this.CameraInformationTabPage = new System.Windows.Forms.TabPage();
            this.CameraInformationUserControl = new CameraToolset.UserControls.CameraInformationUserControl();
            this.IntelligentFocusTabPage = new System.Windows.Forms.TabPage();
            this.MainContainerTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.CameraListGroupBox = new System.Windows.Forms.GroupBox();
            this.CameraListTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ListOfCamerasListBox = new System.Windows.Forms.ListBox();
            this.RefreshCameraListButton = new System.Windows.Forms.Button();
            this.ImageGroupBox = new System.Windows.Forms.GroupBox();
            this.ImagePictureBox = new System.Windows.Forms.PictureBox();
            this.IntelligentFocusUserControl = new CameraToolset.UserControls.IntelligentFocusUserControl();
            this.ControlPanelTabControl.SuspendLayout();
            this.CameraInformationTabPage.SuspendLayout();
            this.IntelligentFocusTabPage.SuspendLayout();
            this.MainContainerTableLayoutPanel.SuspendLayout();
            this.CameraListGroupBox.SuspendLayout();
            this.CameraListTableLayoutPanel.SuspendLayout();
            this.ImageGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImagePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ControlPanelTabControl
            // 
            this.ControlPanelTabControl.Controls.Add(this.CameraInformationTabPage);
            this.ControlPanelTabControl.Controls.Add(this.IntelligentFocusTabPage);
            this.ControlPanelTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ControlPanelTabControl.Location = new System.Drawing.Point(203, 264);
            this.ControlPanelTabControl.Name = "ControlPanelTabControl";
            this.ControlPanelTabControl.SelectedIndex = 0;
            this.ControlPanelTabControl.Size = new System.Drawing.Size(578, 294);
            this.ControlPanelTabControl.TabIndex = 0;
            // 
            // CameraInformationTabPage
            // 
            this.CameraInformationTabPage.Controls.Add(this.CameraInformationUserControl);
            this.CameraInformationTabPage.Location = new System.Drawing.Point(4, 22);
            this.CameraInformationTabPage.Name = "CameraInformationTabPage";
            this.CameraInformationTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.CameraInformationTabPage.Size = new System.Drawing.Size(570, 268);
            this.CameraInformationTabPage.TabIndex = 0;
            this.CameraInformationTabPage.Text = "Camera Information";
            this.CameraInformationTabPage.UseVisualStyleBackColor = true;
            // 
            // CameraInformationUserControl
            // 
            this.CameraInformationUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CameraInformationUserControl.Location = new System.Drawing.Point(3, 3);
            this.CameraInformationUserControl.Name = "CameraInformationUserControl";
            this.CameraInformationUserControl.SelectedCamera = null;
            this.CameraInformationUserControl.Size = new System.Drawing.Size(564, 262);
            this.CameraInformationUserControl.TabIndex = 0;
            // 
            // IntelligentFocusTabPage
            // 
            this.IntelligentFocusTabPage.Controls.Add(this.IntelligentFocusUserControl);
            this.IntelligentFocusTabPage.Location = new System.Drawing.Point(4, 22);
            this.IntelligentFocusTabPage.Name = "IntelligentFocusTabPage";
            this.IntelligentFocusTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.IntelligentFocusTabPage.Size = new System.Drawing.Size(570, 268);
            this.IntelligentFocusTabPage.TabIndex = 1;
            this.IntelligentFocusTabPage.Text = "Intelligent Focus";
            this.IntelligentFocusTabPage.UseVisualStyleBackColor = true;
            // 
            // MainContainerTableLayoutPanel
            // 
            this.MainContainerTableLayoutPanel.ColumnCount = 2;
            this.MainContainerTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.MainContainerTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainContainerTableLayoutPanel.Controls.Add(this.ControlPanelTabControl, 1, 1);
            this.MainContainerTableLayoutPanel.Controls.Add(this.CameraListGroupBox, 0, 0);
            this.MainContainerTableLayoutPanel.Controls.Add(this.ImageGroupBox, 1, 0);
            this.MainContainerTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainContainerTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.MainContainerTableLayoutPanel.Name = "MainContainerTableLayoutPanel";
            this.MainContainerTableLayoutPanel.RowCount = 2;
            this.MainContainerTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainContainerTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.MainContainerTableLayoutPanel.Size = new System.Drawing.Size(784, 561);
            this.MainContainerTableLayoutPanel.TabIndex = 3;
            // 
            // CameraListGroupBox
            // 
            this.CameraListGroupBox.Controls.Add(this.CameraListTableLayoutPanel);
            this.CameraListGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CameraListGroupBox.Location = new System.Drawing.Point(3, 3);
            this.CameraListGroupBox.Name = "CameraListGroupBox";
            this.MainContainerTableLayoutPanel.SetRowSpan(this.CameraListGroupBox, 2);
            this.CameraListGroupBox.Size = new System.Drawing.Size(194, 555);
            this.CameraListGroupBox.TabIndex = 1;
            this.CameraListGroupBox.TabStop = false;
            this.CameraListGroupBox.Text = "Cameras";
            // 
            // CameraListTableLayoutPanel
            // 
            this.CameraListTableLayoutPanel.ColumnCount = 1;
            this.CameraListTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.CameraListTableLayoutPanel.Controls.Add(this.ListOfCamerasListBox, 0, 0);
            this.CameraListTableLayoutPanel.Controls.Add(this.RefreshCameraListButton, 0, 1);
            this.CameraListTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CameraListTableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.CameraListTableLayoutPanel.Name = "CameraListTableLayoutPanel";
            this.CameraListTableLayoutPanel.RowCount = 2;
            this.CameraListTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.CameraListTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.CameraListTableLayoutPanel.Size = new System.Drawing.Size(188, 536);
            this.CameraListTableLayoutPanel.TabIndex = 6;
            // 
            // ListOfCamerasListBox
            // 
            this.ListOfCamerasListBox.DisplayMember = "Name";
            this.ListOfCamerasListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListOfCamerasListBox.FormattingEnabled = true;
            this.ListOfCamerasListBox.IntegralHeight = false;
            this.ListOfCamerasListBox.Location = new System.Drawing.Point(3, 3);
            this.ListOfCamerasListBox.Name = "ListOfCamerasListBox";
            this.ListOfCamerasListBox.ScrollAlwaysVisible = true;
            this.ListOfCamerasListBox.Size = new System.Drawing.Size(182, 501);
            this.ListOfCamerasListBox.TabIndex = 3;
            this.ListOfCamerasListBox.SelectedIndexChanged += new System.EventHandler(this.ListOfCamerasListBox_SelectedIndexChanged);
            // 
            // RefreshCameraListButton
            // 
            this.RefreshCameraListButton.AutoSize = true;
            this.RefreshCameraListButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.RefreshCameraListButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RefreshCameraListButton.Location = new System.Drawing.Point(3, 510);
            this.RefreshCameraListButton.Name = "RefreshCameraListButton";
            this.RefreshCameraListButton.Size = new System.Drawing.Size(182, 23);
            this.RefreshCameraListButton.TabIndex = 3;
            this.RefreshCameraListButton.Text = "Refresh";
            this.RefreshCameraListButton.UseVisualStyleBackColor = true;
            this.RefreshCameraListButton.Click += new System.EventHandler(this.RefreshCameraListButton_Click);
            // 
            // ImageGroupBox
            // 
            this.ImageGroupBox.Controls.Add(this.ImagePictureBox);
            this.ImageGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageGroupBox.Location = new System.Drawing.Point(203, 3);
            this.ImageGroupBox.Name = "ImageGroupBox";
            this.ImageGroupBox.Size = new System.Drawing.Size(578, 255);
            this.ImageGroupBox.TabIndex = 2;
            this.ImageGroupBox.TabStop = false;
            this.ImageGroupBox.Text = "Image";
            // 
            // ImagePictureBox
            // 
            this.ImagePictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImagePictureBox.Image = global::CameraToolset.Properties.Resources.Camera;
            this.ImagePictureBox.Location = new System.Drawing.Point(3, 16);
            this.ImagePictureBox.Name = "ImagePictureBox";
            this.ImagePictureBox.Size = new System.Drawing.Size(572, 236);
            this.ImagePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ImagePictureBox.TabIndex = 0;
            this.ImagePictureBox.TabStop = false;
            // 
            // IntelligentFocusUserControl
            // 
            this.IntelligentFocusUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IntelligentFocusUserControl.Location = new System.Drawing.Point(3, 3);
            this.IntelligentFocusUserControl.Name = "IntelligentFocusUserControl";
            this.IntelligentFocusUserControl.Size = new System.Drawing.Size(564, 262);
            this.IntelligentFocusUserControl.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.MainContainerTableLayoutPanel);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Camera Toolset";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ControlPanelTabControl.ResumeLayout(false);
            this.CameraInformationTabPage.ResumeLayout(false);
            this.IntelligentFocusTabPage.ResumeLayout(false);
            this.MainContainerTableLayoutPanel.ResumeLayout(false);
            this.CameraListGroupBox.ResumeLayout(false);
            this.CameraListTableLayoutPanel.ResumeLayout(false);
            this.CameraListTableLayoutPanel.PerformLayout();
            this.ImageGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ImagePictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl ControlPanelTabControl;
        private System.Windows.Forms.TabPage CameraInformationTabPage;
        private UserControls.CameraInformationUserControl CameraInformationUserControl;
        private System.Windows.Forms.TableLayoutPanel MainContainerTableLayoutPanel;
        private System.Windows.Forms.GroupBox CameraListGroupBox;
        private System.Windows.Forms.TableLayoutPanel CameraListTableLayoutPanel;
        private System.Windows.Forms.ListBox ListOfCamerasListBox;
        private System.Windows.Forms.Button RefreshCameraListButton;
        private System.Windows.Forms.GroupBox ImageGroupBox;
        private System.Windows.Forms.PictureBox ImagePictureBox;
        private System.Windows.Forms.TabPage IntelligentFocusTabPage;
        private UserControls.IntelligentFocusUserControl IntelligentFocusUserControl;
    }
}

