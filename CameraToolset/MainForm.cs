﻿using CameraToolset.Forms;
using CameraToolset.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace CameraToolset
{
    public partial class MainForm : Form
    {
        private OperationResult _initialisationResult = null;
        private List<Camera> _cameras = new List<Camera>();
        private Camera _selectedCamera = null;

        public Camera SelectedCamera
        {
            get
            {
                return _selectedCamera;
            }
            set
            {
                _selectedCamera = value;
                CameraInformationUserControl.SelectedCamera = _selectedCamera;
                IntelligentFocusUserControl.SelectedCamera = _selectedCamera;
            }
        }

        public MainForm()
        {
            InitializeComponent();

            IntelligentFocusUserControl.PictureTakenHandler += HandleTakenPicture;

            _initialisationResult = OperationResultHandler.HandleOperationResult(CanonSDK.Initialise());

            if (_initialisationResult.IsSuccess)
            {
                RefreshListOfCameras();
            }
            else
            {
                // SDK not initialised, list of cameras cannot be retrieved
            }

            RefreshCameraListButton.Enabled = _initialisationResult.IsSuccess;
        }

        private void RefreshListOfCameras()
        {
            if (SelectedCamera != null)
            {
                CanonSDK.CloseCameraSession(SelectedCamera);
            }
            else
            {
                // nothing to disconnect
            }

            SelectedCamera = null;

            _cameras.Clear();
            ListOfCamerasListBox.DataSource = _cameras;


            var cameraListRetrievalResult = OperationResultHandler.HandleOperationResult(
                CanonSDK.GetCameraListAndAssignEventHandlers(
                    out _cameras,
                    CameraInformationUserControl.HandleChangingCameraAperture,
                    CameraInformationUserControl.HandleChangingCameraIso,
                    CameraInformationUserControl.HandleChangingCameraOutputDevice,
                    CameraInformationUserControl.HandleChangingImageQuality,
                    CameraInformationUserControl.HandleChangingJpegQuality,
                    IntelligentFocusUserControl.HandleChangingDirectoryItem));

            if (cameraListRetrievalResult.IsSuccess)
            {
                // successful result is not too interesting in this case
            }
            else
            {
                // no cameras found
            }

            ListOfCamerasListBox.DataSource = _cameras;
        }

        private void MainForm_FormClosing(Object sender, FormClosingEventArgs e)
        {
            if (_initialisationResult != null
                && _initialisationResult.IsSuccess)
            {
                // no need to cater for successful/unsuccessful results
                CanonSDK.Terminate();
            }
            else
            {
                // nothing to terminate
            }
        }

        private void ListOfCamerasListBox_SelectedIndexChanged(Object sender, EventArgs e)
        {
            if (ListOfCamerasListBox.SelectedIndex >= 0
                && ListOfCamerasListBox.SelectedIndex < _cameras.Count)
            {
                var selectedCamera = ListOfCamerasListBox.SelectedValue as Camera;

                if (selectedCamera != SelectedCamera)
                {
                    if (SelectedCamera != null)
                    {
                        var sessionCloseResult = CanonSDK.CloseCameraSession(SelectedCamera);

                        if (sessionCloseResult.IsSuccess)
                        {
                            // camera session closed properly
                        }
                        else
                        {
                            // still good, probably no session was open
                        }
                    }
                    else
                    {
                        // no previously selected camera
                    }

                    CameraInformationUserControl.SelectedCamera = null;

                    selectedCamera.Volumes.Clear();

                    SelectedCamera = selectedCamera;

                    var sessionOpenResult = OperationResultHandler.HandleOperationResult(CanonSDK.OpenCameraSession(SelectedCamera));

                    if (sessionOpenResult.IsSuccess)
                    {
                        var cameraDetailsRetrievalResult = OperationResultHandler.HandleOperationResult(CanonSDK.RetrieveCameraDetails(SelectedCamera));

                        if (cameraDetailsRetrievalResult.IsSuccess)
                        {
                            // great
                        }
                        else
                        {
                            // camera details could not be retrieved
                        }
                    }
                    else
                    {
                        // session could not be opened
                    }
                }
                else
                {
                    // same camera selected
                }
            }
        }

        private void RefreshCameraListButton_Click(object sender, EventArgs e)
        {
            RefreshListOfCameras();
        }

        private void HandleTakenPicture(Image picture)
        {
            ImagePictureBox.Image = picture;
        }
    }
}
